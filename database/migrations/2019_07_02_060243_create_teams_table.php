<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->increments('id');
            $table->text('slug');
            $table->string('name');
            $table->string('email');
            $table->string('phone', 20)->nullable();
            $table->string('designation');
            $table->longText('about')->nullable();
            $table->string('tag_line')->nullable();
            $table->string('important_skills')->nullable();
            $table->string('avatar', 64)->nullable();
            $table->text('facebook_link')->nullable();
            $table->text('githtb_link')->nullable();
            $table->text('stackoverflow_link')->nullable();
            $table->text('linkedin_link')->nullable();
            $table->enum('status', ['active', 'inactive'])->default('inactive');
            $table->integer('sort')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teams');
    }
}
