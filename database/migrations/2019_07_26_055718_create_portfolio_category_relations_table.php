<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortfolioCategoryRelationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portfolio_category_relations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('portfolio_id');
            $table->integer('category_id');
            $table->timestamps();
        });

        Schema::create('portfolio_technology_relations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('portfolio_id');
            $table->integer('technology_id');
            $table->timestamps();
        });

        Schema::create('portfolio_images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('portfolio_id');
            $table->string('image', 64);
            $table->timestamps();
        });

        Schema::create('portfolio_features', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('portfolio_id');
            $table->string('title');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portfolio_category_relations');
        Schema::dropIfExists('portfolio_technology_relations');
        Schema::dropIfExists('portfolio_images');
        Schema::dropIfExists('portfolio_features');
    }
}
