<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('short_name');
            $table->string('address');
            $table->string('email');
            $table->string('phone');
            $table->string('phone_2');
            $table->string('phone_3');
            $table->string('skype');
            $table->text('map_url');
            $table->string('home_title');
            $table->text('home_details');
            $table->text('facebook_link')->nullable();
            $table->text('twitter_link')->nullable();
            $table->text('githtb_link')->nullable();
            $table->text('stackoverflow_link')->nullable();
            $table->text('linkedin_link')->nullable();
            $table->text('pinterest_link')->nullable();
            $table->text('youtube_link')->nullable();
            $table->text('instagram_link')->nullable();
            $table->string('logo')->nullable();
            $table->string('cover_image')->nullable();
            $table->string('company_profile_pdf')->nullable();
            $table->string('status', 32);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
