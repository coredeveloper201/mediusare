<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortfoliosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portfolios', function (Blueprint $table) {
            $table->increments('id');
            $table->text('slug');
            $table->string('title');
            $table->string('service_provided')->nullable();
            $table->string('industry')->nullable();
            $table->longText('company_about')->nullable();
            $table->text('solution')->nullable();
            $table->text('impact')->nullable();
            $table->string('client_name');
            $table->date('date')->nullable();
            $table->text('url')->nullable();
            $table->string('image')->nullable();
            $table->string('youtube_id')->nullable();
            $table->integer('sort')->default(0);
            $table->enum('status', ['active', 'inactive'])->default('inactive');
            $table->timestamps();
        });

        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('description')->nullable();
            $table->enum('status', ['active', 'inactive'])->default('inactive');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portfolios');
        Schema::dropIfExists('categories');
    }
}
