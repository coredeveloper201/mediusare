<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PortfolioCategoryRelation extends Model
{
    protected $table = 'portfolio_category_relations';

    public function category(){
        return $this->belongsTo('App\Models\Category', 'category_id', 'id');
    }

    public function portfolios(){
        return $this->belongsTo('App\Models\Portfolio', 'portfolio_id', 'id');
    }
}
