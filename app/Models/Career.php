<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Career extends Model
{
    use Sluggable;

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    protected $fillable = ['slug', 'title', 'vacancy', 'job_context', 'job_responsibilities', 'employment_status','image', 'experience_requirements', 'educational_requirements', 'additional_requirements', 'other_benefits', 'read_before_apply', 'salary', 'apply_email', 'vacancy','vacancy','vacancy', 'deadline', 'status'];
}
