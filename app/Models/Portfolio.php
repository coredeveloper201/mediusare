<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{
    use Sluggable;

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function category(){
        return $this->belongsTo('App\Models\Category', 'category_id', 'id');
    }

    public function getCategories(){
        return $this->hasMany('App\Models\PortfolioCategoryRelation', 'portfolio_id', 'id');
    }

    public function getTechnologies(){
        return $this->hasMany('App\Models\PortfolioTechnologyRelation', 'portfolio_id', 'id');
    }

    public function getFeatures(){
        return $this->hasMany('App\Models\PortfolioFeature', 'portfolio_id', 'id');
    }

    protected $fillable = [
        'sort'
    ];


    public function getImages(){
        return $this->hasMany('App\Models\PortfolioImage', 'portfolio_id', 'id');
    }




}
