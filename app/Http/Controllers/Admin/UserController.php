<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Validator;
use Image;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::orderBy('sort', 'ASC')->get();
        return view('admin.users', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.user_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'role' => Rule::in(['admin', 'editor']),
            'name' => 'required|max:191',
            'email' => 'required|email|unique:users|max:191',
            'password' => 'required|string|min:6|confirmed',
            'status' => Rule::in(['active', 'deactivate']),
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $user  = new User();

        if($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = $image->getClientOriginalExtension();
            $fileName = time() . "." . $imageName;
            $directory = 'media/user/';
            $imageUrl = $directory.$fileName;
            Image::make($image)->resize(300, 300)->save($imageUrl);
            $user->image = $fileName;
        }

        $user->role      = $request->role;
        $user->name      = $request->name;
        $user->email     = $request->email;
        $user->phone     = $request->phone;
        $user->password  = Hash::make($request->password);
        $user->status    = $request->status;
        $user->save();
        return redirect(route('user.index'))->with('success', 'User save successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('admin.user_edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'role' => Rule::in(['admin', 'editor']),
            'name' => 'required|max:191',
            'status' => Rule::in(['active', 'deactivate']),
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $user = User::find($id);

        if($request->hasFile('image')) {
            if (!file_exists(public_path('media/user/'.$user->image))){
                $user->image = null;
            }else{
                unlink('media/user/'. $user->image);
            }
            $image = $request->file('image');
            $imageName = $image->getClientOriginalExtension();
            $fileName = time() . "." . $imageName;
            $directory = 'media/user/';
            $imageUrl = $directory.$fileName;
            Image::make($image)->resize(300, 300)->save($imageUrl);
            $user->image = $fileName;
        }

        $user->role    = $request->role;
        $user->name    = $request->name;
        $user->status  = $request->status;
        $user->save();
        return redirect(route('user.index'))->with('success', 'User update successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if ($user->image){
            if (!file_exists(public_path('media/user/'.$user->image))){
                $user->image = null;
            }else{
                unlink('media/user/'. $user->image);
            }
        }
        $user->delete();
        return redirect()->back()->with('error', 'User deleted!!');
    }

    public function sortable(Request $request){
        $users = User::all();
        foreach ($users as $user) {
            $user->timestamps = false; // To disable update_at field updation
            $id = $user->id;
            foreach ($request->order as $order) {
                if ($order['id'] == $id) {
                    $user->update(['sort' => $order['position']]);
                }
            }
        }

        return response('Update Successfully.', 200);
    }
}
