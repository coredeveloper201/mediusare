<?php

namespace App\Http\Controllers\Admin;

use App\Models\Contact;
use App\Models\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class DashboardController extends Controller
{
    public function index(){
        return view('admin.index');
    }

    public function setting(){
        $setting = Setting::first();
        return view('admin.setting', compact('setting'));
    }

    public function settingStore(Request $request){
        $data = $request->all();
        $validator = Validator::make($data, [
            'name'         => 'required|max:191',
            'short_name'   => 'required|max:10',
            'email'        => 'required|email',
            'phone'        => 'required|max:20',
            'phone_2'      => 'required|max:20',
            'phone_3'      => 'required|max:20',
            'skype'        => 'required|max:191',
            'address'      => 'required|max:300',
            'map_url'      => 'required',
            'home_title'   => 'required',
            'home_details' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        Setting::updateOrCreate(
            ['status' => 'update'],
            [
                'name'               => $request->name,
                'short_name'         => $request->short_name,
                'address'            => $request->address,
                'email'              => $request->email,
                'phone'              => $request->phone,
                'phone_2'            => $request->phone_2,
                'phone_3'            => $request->phone_3,
                'skype'              => $request->skype,
                'map_url'            => $request->map_url,
                'home_title'         => $request->home_title,
                'home_details'       => $request->home_details,
                'facebook_link'      => $request->facebook_link,
                'twitter_link'       => $request->twitter_link,
                'githtb_link'        => $request->githtb_link,
                'stackoverflow_link' => $request->stackoverflow_link,
                'linkedin_link'      => $request->linkedin_link,
                'pinterest_link'     => $request->pinterest_link,
                'youtube_link'       => $request->youtube_link,
                'instagram_link'     => $request->instagram_link,
            ]);

        return redirect()->back()->with('success', 'Setting update successfully!');
    }

    public function contacts(){
        $contacts = Contact::orderBy('created_at', 'DESC')->get();
        return view('admin.contacts', compact('contacts'));
    }

    public function contactDelete($id){
        Contact::find($id)->delete();
        return redirect()->back()->with('success', 'Contact delete successfully!');
    }

}
