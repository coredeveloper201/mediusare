<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\Portfolio;
use App\Models\PortfolioCategoryRelation;
use App\Models\PortfolioFeature;
use App\Models\PortfolioImage;
use App\Models\PortfolioTechnologyRelation;
use App\Models\Technology;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Image;
use Validator;

class PortfolioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $portfolios = Portfolio::orderBy('sort', 'ASC')->get();
        return view('admin.portfolios', compact('portfolios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::get();
        $technologies = Technology::get();
        return view('admin.portfolio_create', compact('categories', 'technologies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'title' => 'required|max:255',
//            'category' => 'required|numeric|max:255',
//            'short_description' => 'required|max:300',
//            'description' => 'required',
//            'skills' => 'required|max:255',
            'client_name' => 'required|max:255',
            'url' => 'required|url|max:255',
            'image' => 'required|image',
            'status' => 'required',
            'showcase' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $portfolio  = new Portfolio();
        if($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = $image->getClientOriginalExtension();
            $fileName = time() . "." . $imageName;
            $directory = 'media/portfolio/';
            $imageUrl = $directory.$fileName;
            Image::make($image)->resize(1903, 600)->save($imageUrl);
            $portfolio->image = $fileName;
        }
        $portfolio->title             = $request->title;
        $portfolio->client_name       = $request->client_name;
        $portfolio->service_provided  = $request->service_provided;
//        $portfolio->industry          = $request->industry;
        $portfolio->company_about     = $request->company_about;
        $portfolio->solution          = $request->solution;
        $portfolio->impact            = $request->impact;
        $portfolio->date              = date('Y-m-d');
        $portfolio->url               = $request->url;
        $portfolio->youtube_id        = $request->youtube_id;
        $portfolio->status            = $request->status;
        $portfolio->showcase          = $request->showcase;
        //return $portfolio;
        if ($portfolio->save()){
            if (!empty($data['portfolio_feature'])) {
                //array filter for zero empty value check
                $portfolioFeature = $data['portfolio_feature'];
                $portfolioFeature = !empty($portfolioFeature) ? array_values(array_filter($portfolioFeature)) : array();
                $featureData = [];
                foreach ($portfolioFeature as $k => $featureTitle) {
                    $featureData[] = [
                        'portfolio_id' => $portfolio->id,
                        'title' => $featureTitle,
                    ];
                }
                if (!empty($featureData)) {
                    PortfolioFeature::insert($featureData);
                }
            }

            if (!empty($data['category'])) {
                $categoryData = [];
                foreach ($data['category'] as $k => $categoryId) {
                    $categoryData[] = [
                        'portfolio_id' => $portfolio->id,
                        'category_id' => $categoryId,
                    ];
                }
                if (!empty($categoryData)) {
                    PortfolioCategoryRelation::insert($categoryData);
                }
            }

            if (!empty($data['technology'])) {
                $technologyData = [];
                foreach ($data['technology'] as $k => $technologyId) {
                    $technologyData[] = [
                        'portfolio_id' => $portfolio->id,
                        'technology_id' => $technologyId,
                    ];
                }
                if (!empty($technologyData)) {
                    PortfolioTechnologyRelation::insert($technologyData);
                }
            }


            if($request->hasfile('portfolio_image'))
            {
                $portfolioImages = [];
                foreach($request->file('portfolio_image') as $key => $image)
                {
                    $name=$image->getClientOriginalName();
                    $image->move(public_path().'/media/portfolio-img/',time().'_'. $name);
                    $portfolioImages[] = [
                        'portfolio_id' => $portfolio->id,
                        'image' => time().'_'.$name,
                    ];
                }

                if (!empty($portfolioImages)) {
                    PortfolioImage::insert($portfolioImages);
                }
            }

        }
        return redirect()->back()->with('success', 'Portfolio save successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $portfolio = Portfolio::find($id);
        return response()->json([
            'message' => 'success',
            'obj' => $portfolio,
            'category_name' => isset($portfolio->category->title) ? $portfolio->category->title:'',
//            'date' => date('F d, Y', strtotime($portfolio->date)),
            'image' => asset('media/portfolio/'.$portfolio->image),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $portfolio = Portfolio::find($id);
        $portfolio->getCategories = $portfolio->getCategories->pluck('category_id')->toArray();
        $portfolio->getTechnologies = $portfolio->getTechnologies->pluck('technology_id')->toArray();
        $categories = Category::get();
        $technologies = Technology::get();

        return view('admin.portfolio_edit', compact('portfolio', 'categories', 'technologies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'title' => 'required|max:255',
            'client_name' => 'required|max:255',
            'url' => 'required|max:255',
            'status' => 'required',
            'showcase' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $portfolio  = Portfolio::findOrFail($id);
        if($request->hasFile('image')) {
            if (!file_exists(public_path('media/portfolio/'.$portfolio->image))){
                $portfolio->image = null;
            }else{
                unlink('media/portfolio/'. $portfolio->image);
            }
            $image = $request->file('image');
            $imageName = $image->getClientOriginalExtension();
            $fileName = time() . "." . $imageName;
            $directory = 'media/portfolio/';
            $imageUrl = $directory.$fileName;
            Image::make($image)->resize(1903, 600)->save($imageUrl);
            $portfolio->image = $fileName;
        }
        $portfolio->title             = $request->title;
        $portfolio->client_name       = $request->client_name;
        $portfolio->service_provided  = $request->service_provided;
//        $portfolio->industry          = $request->industry;
        $portfolio->company_about     = $request->company_about;
        $portfolio->solution          = $request->solution;
        $portfolio->impact            = $request->impact;
        $portfolio->date              = date('Y-m-d');
        $portfolio->url               = $request->url;
        $portfolio->youtube_id        = $request->youtube_id;
        $portfolio->status            = $request->status;
        $portfolio->showcase          = $request->showcase;
        if ($portfolio->save()){
            if (!empty($data['portfolio_feature'])) {
                PortfolioFeature::where('portfolio_id', $id)->delete();
                $portfolioFeature = $data['portfolio_feature'];
                $portfolioFeature = !empty($portfolioFeature) ? array_values(array_filter($portfolioFeature)) : array();
                $featureData = [];
                foreach ($portfolioFeature as $k => $featureTitle) {
                    $featureData[] = [
                        'portfolio_id' => $portfolio->id,
                        'title' => $featureTitle,
                    ];
                }
                if (!empty($featureData)) {
                    PortfolioFeature::insert($featureData);
                }
            }

            if (!empty($data['category'])) {
                PortfolioCategoryRelation::where('portfolio_id', $id)->delete();
                $categoryData = [];
                foreach ($data['category'] as $k => $categoryId) {
                    $categoryData[] = [
                        'portfolio_id' => $portfolio->id,
                        'category_id' => $categoryId,
                    ];
                }
                if (!empty($categoryData)) {
                    PortfolioCategoryRelation::insert($categoryData);
                }
            }

            if (!empty($data['technology'])) {
                PortfolioTechnologyRelation::where('portfolio_id', $id)->delete();
                $technologyData = [];
                foreach ($data['technology'] as $k => $technologyId) {
                    $technologyData[] = [
                        'portfolio_id' => $portfolio->id,
                        'technology_id' => $technologyId,
                    ];
                }
                if (!empty($technologyData)) {
                    PortfolioTechnologyRelation::insert($technologyData);
                }
            }

            if($request->hasfile('portfolio_image'))
            {
                $portfolioImages = [];
                foreach($request->file('portfolio_image') as $key => $image)
                {
                    $name=$image->getClientOriginalName();
                    $image->move(public_path().'/media/portfolio-img/',time().'_'. $name);
                    $portfolioImages[] = [
                        'portfolio_id' => $portfolio->id,
                        'image' => time().'_'.$name,
                    ];
                }

                if (!empty($portfolioImages)) {
                    PortfolioImage::insert($portfolioImages);
                }
            }

        }
        return redirect(route('portfolio.index'))->with('success', 'Portfolio update successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $portfolio = Portfolio::find($id);
        PortfolioCategoryRelation::where('portfolio_id', $id)->delete();
        PortfolioFeature::where('portfolio_id', $id)->delete();
        PortfolioTechnologyRelation::where('portfolio_id', $id)->delete();

        $portfolioImgs = PortfolioImage::where('portfolio_id', $id)->get();
        if (!empty($portfolioImgs)){
            foreach ($portfolioImgs as $img){
                if ($img->image){
                    if (!file_exists(public_path('media/portfolio-img/'.$img->image))){
                        $img->image = null;
                    }else{
                        unlink('media/portfolio-img/'. $img->image);
                    }
                }
                $img->delete();
            }
        }

        if ($portfolio->image){
            if (!file_exists(public_path('media/portfolio/'.$portfolio->image))){
                $portfolio->image = null;
            }else{
                unlink('media/portfolio/'. $portfolio->image);
            }
        }
        $portfolio->delete();
        return redirect()->back()->with('error', 'Portfolio deleted');
    }

    public function sortable(Request $request){
        $portfolios = Portfolio::all();
        foreach ($portfolios as $portfolio) {
            $portfolio->timestamps = false; // To disable update_at field updation
            $id = $portfolio->id;

            foreach ($request->order as $order) {
                if ($order['id'] == $id) {
                    $portfolio->update(['sort' => $order['position']]);
                }
            }
        }

        return response('Update Successfully.', 200);
    }

    public function portfolioImageDelete($id){
        $portfolioImage = PortfolioImage::find($id);
        if (!file_exists(public_path('media/portfolio-img/'.$portfolioImage->image))){
            $portfolioImage->image = null;
        }else{
            unlink('media/portfolio-img/'. $portfolioImage->image);
        }

        $portfolioImage->delete();
        return redirect()->back();
    }

}

