<?php

namespace App\Http\Controllers\Admin;

use App\Models\Feature;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Image;

class FeatureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $features = Feature::get();
        return view('admin.features', compact('features'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.feature_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'title' => 'required|max:191',
            'short_description' => 'required|max:300',
            'description' => 'required',
            'image' => 'required|image',
            'status' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $feature  = new Feature();
        if($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = $image->getClientOriginalExtension();
            $fileName = time() . "." . $imageName;
            $directory = 'media/feature/';
            $imageUrl = $directory.$fileName;
            Image::make($image)->resize(1920, 750)->save($imageUrl);
            $feature->image = $fileName;
        }
        $feature->title             = $request->title;
        $feature->short_description = $request->short_description;
        $feature->description       = $request->description;
        $feature->status            = $request->status;
        $feature->save();
        return redirect()->back()->with('success', 'Feature save successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $feature = Feature::find($id);
        return view('admin.feature_edit', compact('feature'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'title' => 'required|max:191',
            'short_description' => 'required|max:300',
            'description' => 'required',
//            'image' => 'required|image',
            'status' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $feature = Feature::find($id);
        if($request->hasFile('image')) {
            if (!file_exists(public_path('media/feature/'.$feature->iamge))){
                $feature->iamge = null;
            }else{
                unlink('media/feature/'. $feature->iamge);
            }
            $image = $request->file('image');
            $imageName = $image->getClientOriginalExtension();
            $fileName = time() . "." . $imageName;
            $directory = 'media/feature/';
            $imageUrl = $directory.$fileName;
            Image::make($image)->resize(1920, 750)->save($imageUrl);
            $feature->image = $fileName;
        }
        $feature->title             = $request->title;
        $feature->short_description = $request->short_description;
        $feature->description       = $request->description;
        $feature->status            = $request->status;
        $feature->save();
        return redirect(route('feature.index'))->with('success', 'Feature update successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $feature = Feature::find($id);

        if ($feature->iamge){
            if (!file_exists(public_path('media/feature/'.$feature->iamge))){
                $feature->iamge = null;
            }else{
                unlink('media/feature/'. $feature->iamge);
            }
        }
        $feature->delete();
        return redirect()->back()->with('error', 'Feature deleted!!');
    }
}
