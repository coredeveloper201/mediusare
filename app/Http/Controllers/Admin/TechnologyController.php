<?php

namespace App\Http\Controllers\Admin;

use App\Models\Technology;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Image;

class TechnologyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $technologies = Technology::orderBy('sort', 'ASC')->get();
        return view('admin.technologies', compact('technologies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.technology_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'title' => 'required|max:191',
            'image' => 'required|image',
            'status' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $technology  = new Technology();
        if($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = $image->getClientOriginalExtension();
            $fileName = time() . "." . $imageName;
            $directory = 'media/technology/';
            $imageUrl = $directory.$fileName;
            Image::make($image)->resize(80, 30)->save($imageUrl);
            $technology->image = $fileName;
        }
        $technology->title   = $request->title;
        $technology->status  = $request->status;
        $technology->save();
        return redirect()->back()->with('success', 'Technology save successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $technology = Technology::find($id);
        return view('admin.technology_edit', compact('technology'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'title' => 'required|max:191',
            'status' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $technology = Technology::find($id);
        if($request->hasFile('image')) {
            if (!file_exists(public_path('media/technology/'.$technology->image))){
                $technology->image = null;
            }else{
                unlink('media/technology/'. $technology->image);
            }
            $image = $request->file('image');
            $imageName = $image->getClientOriginalExtension();
            $fileName = time() . "." . $imageName;
            $directory = 'media/technology/';
            $imageUrl = $directory.$fileName;
            Image::make($image)->resize(80, 30)->save($imageUrl);
            $technology->image = $fileName;
        }
        $technology->title   = $request->title;
        $technology->status  = $request->status;
        $technology->save();
        return redirect(route('technology.index'))->with('success', 'Technology update successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $technology = Technology::find($id);

        if ($technology->image){
            if (!file_exists(public_path('media/technology/'.$technology->image))){
                $technology->image = null;
            }else{
                unlink('media/technology/'. $technology->image);
            }
        }
        $technology->delete();
        return redirect()->back()->with('success', 'Technology delete successfully');
    }

    public function sortable(Request $request){
        $technologies = Technology::all();
        foreach ($technologies as $technology) {
            $technology->timestamps = false; // To disable update_at field updation
            $id = $technology->id;
            foreach ($request->order as $order) {
                if ($order['id'] == $id) {
                    $technology->update(['sort' => $order['position']]);
                }
            }
        }
        return response('Update Successfully.', 200);
    }
}
