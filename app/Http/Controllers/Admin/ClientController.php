<?php

namespace App\Http\Controllers\Admin;

use App\Models\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Image;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Client::orderBy('sort', 'ASC')->get();
        return view('admin.clients', compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.client_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'title' => 'required|max:191',
            'image' => 'required|image',
            'status' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $client  = new Client();
        if($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = $image->getClientOriginalExtension();
            $fileName = time() . "." . $imageName;
            $directory = 'media/client/';
            $imageUrl = $directory.$fileName;
            Image::make($image)->resize(80, 30)->save($imageUrl);
            $client->image = $fileName;
        }
        $client->title   = $request->title;
        $client->status  = $request->status;
        $client->save();
        return redirect()->back()->with('success', 'client save successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client = Client::find($id);
        return view('admin.client_edit', compact('client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'title' => 'required|max:191',
            'image' => 'required|image',
            'status' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $client = Client::find($id);
        if($request->hasFile('image')) {
            if (!file_exists(public_path('media/client/'.$client->image))){
                $client->image = null;
            }else{
                unlink('media/client/'. $client->image);
            }

            $image = $request->file('image');
            $imageName = $image->getClientOriginalExtension();
            $fileName = time() . "." . $imageName;
            $directory = 'media/client/';
            $imageUrl = $directory.$fileName;
            Image::make($image)->resize(80, 30)->save($imageUrl);
            $client->image = $fileName;
        }
        $client->title   = $request->title;
        $client->status  = $request->status;
        $client->save();
        return redirect()->back()->with('success', 'Client Update successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client = Client::find($id);
        if ($client->image){
            if (!file_exists(public_path('media/client/'.$client->image))){
                $client->image = null;
            }else{
                unlink('media/client/'. $client->image);
            }
        }
        $client->delete();
        return redirect()->back()->with('success', 'client delete successfully');
    }

    public function sortable(Request $request){
        $clients = Client::all();
        foreach ($clients as $client) {
            $client->timestamps = false; // To disable update_at field updation
            $id = $client->id;
            foreach ($request->order as $order) {
                if ($order['id'] == $id) {
                    $client->update(['sort' => $order['position']]);
                }
            }
        }
        return response('Update Successfully.', 200);
    }
}
