@extends('website.master')

@section('title')
    Mediusware | Services
@endsection

@section('content')
    <!-- Start page-top section -->
    <section class="page-top-section">
        <div class="container">
            <div class="row justify-content-between align-items-center">
                <div class="col-lg-6 col-md-6">
                    <h1 class="text-white">Service</h1>
                </div>
                <div class="col-lg-6  col-md-6 page-top-nav">
                    <div>
                        <a href="{!! url('/home') !!}">Home</a>
                        <span class="lnr lnr-arrow-right"></span>
                        <a href="teams.html">Service</a>
                    </div>
                </div>
            </div>
        </div>



    </section>
    <!-- End page-top section -->

    <!-- Start unique-feature Area -->
    <section class="unique-feature-area section-gap">
        <div class="container">

            <div class="row justify-content-center section-title-wrap">
                <div class="col-lg-12">
                    <div class="title-img">
                        <img src="{!! asset('/assets') !!}/img/title-icon.png" alt="">
                    </div>
                    <h1>Our All <span class="text-info">Service</span> Here</h1><br>
                    <p>Our most stylish homegrown talents on their passions</p>
                </div>
            </div>


            <div class="row">
                <div class="col-md-4">
                    <div class="service-item ">
                        <i class="fa fa-search-plus"></i>
                        <h3>Website From Scratch</h3>
                        <p>We will be create your website from scratch. If you have idea about what you need to do, share us, we will deliver your website with perfection</p>
                        <button type="button" class="ron-btn">more</button>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="service-item ">
                        <i class="fa fa-laptop"></i>
                        <h3>PSD to HTML/CSS</h3>
                        <p>If you have ready psd design on photoshop, we can convert that pixel perfect output with html css. Responsive and cross browser will be must.</p>
                        <button type="button" class="ron-btn">more</button>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="service-item ">
                        <i class="fa fa-edit"></i>
                        <h3>PSD to WordPress</h3>
                        <p>If you have ready psd design on photoshop, we can convert that pixel perfect output with WordPress. We will ensure responsiveness.</p>
                        <button type="button" class="ron-btn">more</button>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="service-item ">
                        <i class="fa fa-briefcase"></i>
                        <h3>HTML to WordPress</h3>
                        <p>Have ready already codded html template, we can convert that perfectly with WordPress. We will cover all of your requested functionality.</p>
                        <button type="button" class="ron-btn">more</button>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="service-item ">
                        <i class="fa fa-code-branch"></i>
                        <h3>PSD to WooCommerce</h3>
                        <p>Have ready already codded html template, we can convert that perfectly with WordPress and WooCommerce. We will cover all of your requested functionality.</p>
                        <button type="button" class="ron-btn">more</button>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="service-item ">
                        <i class="fa fa-code-branch"></i>
                        <h3>Laravel Web app</h3>
                        <p>We have talented Laravel app developer. We are ready to convert you idea in to a web app with Laravel.</p>
                        <button type="button" class="ron-btn">more</button>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="service-item ">
                        <i class="fa fa-glass-martini"></i>
                        <h3>Corporate Website</h3>
                        <p>Business websites need not be boring and dull. Enhance your customers’ experience with web design that is both creative and professional.</p>
                        <button type="button" class="ron-btn">more</button>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="service-item ">
                        <i class="fa fa-globe"></i>
                        <h3>Creative Website</h3>
                        <p>Creative websites challenge the norm. Stand out from the millions of websites on the Internet with expert design that is insightful.</p>
                        <button type="button" class="ron-btn">more</button>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="service-item ">
                        <i class="fa fa-shopping-bag"></i>
                        <h3>eCommerce Shop</h3>
                        <p>Website visitors today are sophisticated and expect easy to navigate sites and they have little to no tolerance for a site that doesn't easily provide them with information.</p>
                        <button type="button" class="ron-btn">more</button>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="service-item ">
                        <i class="fa fa-code"></i>
                        <h3>Custom Programming</h3>
                        <p>Custom Programming is about meeting your specific on-line requirements. Configure your website according to your evolving</p>
                        <button type="button" class="ron-btn">more</button>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="service-item ">
                        <i class="fa fa-random"></i>
                        <h3>Responsive Web Design</h3>
                        <p>Your web contents should do more than just inform. Establish credibility and engage your customers with good writing</p>
                        <button type="button" class="ron-btn">more</button>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="service-item ">
                        <i class="fa fa-copyright"></i>
                        <h3>Copyrighting</h3>
                        <p>Today’s Internet users possess a variety of devices from which they can access websites. Ensure yours is friendly to work on</p>
                        <button type="button" class="ron-btn">more</button>
                    </div>
                </div>

            </div>




        </div>
    </section>
    <!-- End unique-feature Area -->

@endsection