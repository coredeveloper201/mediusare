@extends('website.master')

@section('title')
    Mediusware | Features
@endsection

@section('content')
    <!-- Start page-top section -->
    <section class="page-top-section">
        <div class="container">
            <div class="row justify-content-between align-items-center">
                <div class="col-lg-6 col-md-6">
                    <h1 class="text-white">Features</h1>
                </div>
                <div class="col-lg-6  col-md-6 page-top-nav">
                    <div>
                        <a href="{!! url('/home') !!}">Home</a>
                        <span class="lnr lnr-arrow-right"></span>
                        <a href="features.html">Features</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End page-top section -->


    <!-- Start unique-feature Area -->
    <section class="unique-feature-area section-gap">
        <div class="container">
            <div class="row justify-content-center section-title-wrap">
                <div class="col-lg-12">
                    <div class="title-img">
                        <img src="{!! asset('/assets') !!}/img/title-icon.png" alt="">
                    </div>
                    <h1>Features that make it Simple</h1>
                    <p>
                        The first is a non technical method which requires the use of adware removal software. Download free adware and spyware removal
                        software and use advanced tools getting infected.
                    </p>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="active-feature-carusel">
                        <img class="img-fluid" src="{!! asset('/assets') !!}/img/carusel/c4.png" alt="">
                        <img class="img-fluid" src="{!! asset('/assets') !!}/img/carusel/c5.png" alt="">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="feature-list d-flex flex-row">
                        <div class="icon">
                            <img class="img-fluid" src="{!! asset('/assets') !!}/img/feature/f4.png" alt="">
                        </div>
                        <div class="desc ml-40">
                            <a href="#">
                                <h4 class="mb-20">Task Management</h4>
                            </a>
                            <p>
                                For any aspect of self-improvement, there has to be a reason for you to start and keep going. That is your motivation – the
                                reason for doing what you do.
                            </p>
                        </div>
                    </div>
                    <div class="feature-list d-flex flex-row">
                        <div class="icon">
                            <img class="img-fluid" src="{!! asset('/assets') !!}/img/feature/f6.png" alt="">
                        </div>
                        <div class="desc ml-40">
                            <a href="#">
                                <h4 class="mb-20">Time Tracking</h4>
                            </a>
                            <p>
                                If you are an entrepreneur, you know that your success cannot depend on the opinions of others. Like the wind, opinions change…like
                                the weather, opinions change frequently.
                            </p>
                        </div>
                    </div>
                    <div class="feature-list d-flex flex-row">
                        <div class="icon">
                            <img class="img-fluid" src="{!! asset('/assets') !!}/img/feature/f7.png" alt="">
                        </div>
                        <div class="desc ml-40">
                            <a href="#">
                                <h4 class="mb-20">Advanced Reporting</h4>
                            </a>
                            <p>
                                Do you want to be even more successful? Learn to love learning and growth. The more effort you put into improving your skills,
                                the bigger the payoff you will get.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End unique-feature Area -->

    <!-- Start cta-section -->
    <section class="cta-section gradient-bg">
        <div class="container">
            <div class="row align-items-center justify-content-center text-center">
                <div class="col-lg-8">
                    <h1 class="text-white mb-5">Fallen in Love with <br>
                        our features? Get a free trial for 14 days!</h1>
                    <div class="cta-btn">
                        <a href="#" class="ct-btn1 mr-sm-3 mb-sm-0 mb-3">Start free trial</a>
                        <a href="#" class="ct-btn2 active">Signup</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End cta-section -->

    <!-- Start important-points section -->
    <section class="important-points-section section-gap">
        <div class="container">
            <div class="row justify-content-center section-title-wrap">
                <div class="col-lg-12">
                    <div class="title-img">
                        <img src="{!! asset('/assets') !!}/img/title-icon.png" alt="">
                    </div>
                    <h1>Some Important Points to be noted</h1>
                    <p>
                        The first is a non technical method which requires the use of adware removal software. Download free adware and spyware removal
                        software and use advanced tools getting infected.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-6 single-points aquablue-bg">
                    <img src="{!! asset('/assets') !!}/img/points/i1.png" alt="">
                    <h4>
                        Search
                        <br> Management
                        <br> System
                    </h4>
                    <p>
                        Staying calm, composed and maintaining strong self esteem in today’s tough environment can be difficult but is not impossible
                        if you follow a few simple guidelines.
                    </p>
                </div>
                <div class="col-lg-3 col-md-6 single-points aquablue-bg">
                    <img src="{!! asset('/assets') !!}/img/points/i2.png" alt="">
                    <h4>
                        Data
                        <br> Synchronization
                        <br> Options
                    </h4>
                    <p>
                        We all live in an age that belongs to the young at heart. Life that is becoming extremely fast, day to day, also asks us
                        to remain physically young. Young at heart.
                    </p>
                </div>
                <div class="col-lg-3 col-md-6 single-points aquablue-bg">
                    <img src="{!! asset('/assets') !!}/img/points/i3.png" alt="">
                    <h4>
                        Team
                        <br> Management
                        <br> Facilities
                    </h4>
                    <p>
                        Increasing prosperity in our lives can be accomplished by having the right frame of mind. The truth is, our thoughts are
                        very powerful. They are capable of influencing.
                    </p>
                </div>
                <div class="col-lg-3 col-md-6 single-points aquablue-bg">
                    <img src="{!! asset('/assets') !!}/img/points/i4.png" alt="">
                    <h4>
                        Schedule
                        <br> Adjustment
                        <br> System
                    </h4>
                    <p>
                        Have you ever had a problem with a burned light? Thanks to the effort of Thomas Edison we no longer need to invent a light
                        bulb. We just go to the store.
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!-- End important-points section -->

@endsection