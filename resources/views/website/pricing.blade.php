@extends('website.master')

@section('title')
    Mediusware | Pricing
@endsection

@section('content')
    <!-- Start page-top section -->
    <section class="page-top-section">
        <div class="container">
            <div class="row justify-content-between align-items-center">
                <div class="col-lg-6 col-md-6">
                    <h1 class="text-white">Pricing Plans</h1>
                </div>
                <div class="col-lg-6  col-md-6 page-top-nav">
                    <div>
                        <a href="{!! url('/home') !!}">Home</a>
                        <span class="lnr lnr-arrow-right"></span>
                        <a href="pricing.html">Pricing Plans</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End page-top section -->

    <!-- Start price section -->
    <section class="price-section section-gap">
        <div class="container">
            <div class="row justify-content-center section-title-wrap">
                <div class="col-lg-12">
                    <div class="title-img">
                        <img src="{!! asset('/assets') !!}/img/title-icon.png" alt="">
                    </div>
                    <h1>Simplistic Pricing helps to choose from</h1>
                    <p>
                        The first is a non technical method which requires the use of adware removal software. Download
                        free adware and spyware removal
                        software and use advanced tools getting infected.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="single-price2">
                        <div class="price-top aquablue-bg relative">
                            <div class="overlay overlay-bg"></div>
                            <h4 class="relative">Free Lifetime</h4>
                            <p class="pt-10 relative">
                                Small business holders
                            </p>
                            <h1 class="pt-40 relative">£0.00</h1>
                        </div>
                        <div class="price-bottom">
                            <ul class="pack-list">
                                <li>02 Users permitted</li>
                                <li>Unlimited Styles for interface</li>
                                <li>New Update Available</li>
                            </ul>
                            <a href="#" class="genric-btn2 mt-30">signup for free</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="single-price2">
                        <div class="price-top aquablue-bg relative">
                            <div class="overlay overlay-bg"></div>
                            <h4 class="relative">Standard</h4>
                            <p class="pt-10 relative">
                                Medium business holders
                            </p>
                            <h1 class="pt-40 relative">£49.00</h1>
                        </div>
                        <div class="price-bottom">
                            <ul class="pack-list">
                                <li>05 Users permitted</li>
                                <li>Unlimited Styles for interface</li>
                                <li>New Update Available</li>
                            </ul>
                            <a href="#" class="genric-btn2 mt-30">signup for free</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="single-price2">
                        <div class="price-top aquablue-bg relative">
                            <div class="overlay overlay-bg"></div>
                            <h4 class="relative">Essential</h4>
                            <p class="pt-10 relative">
                                Large business holders
                            </p>
                            <h1 class="pt-40 relative">£99.00</h1>
                        </div>
                        <div class="price-bottom">
                            <ul class="pack-list">
                                <li>Unlimited Users permitted</li>
                                <li>Unlimited Styles for interface</li>
                                <li>New Update Available</li>
                            </ul>
                            <a href="#" class="genric-btn2 mt-30">signup for free</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End price section -->

    <!-- Start faq-section -->
    <section class="faq-section section-gap">
        <div class="container">
            <div class="row justify-content-center section-title-wrap">
                <div class="col-lg-12">
                    <h1>Simplistic Pricing helps to choose from</h1>
                    <p>
                        The first is a non technical method which requires the use of adware removal software. Download
                        free adware and spyware removal
                        software and use advanced tools getting infected.
                    </p>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="nav-center pb-40 accordian-tab-menu">
                    <ul class="nav nav-pills col-lg-12" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="pills-general-tab" data-toggle="pill" href="#pills-general"
                               role="tab" aria-controls="pills-general" aria-selected="true">General Issues</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-support-tab" data-toggle="pill" href="#pills-support" role="tab"
                               aria-controls="pills-support" aria-selected="false">Support Issues</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-refund-tab" data-toggle="pill" href="#pills-refund" role="tab"
                               aria-controls="pills-refund" aria-selected="false">Refund Issues</a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content col-lg-8" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-general" role="tabpanel" aria-labelledby="pills-general-tab">
                        <dl class="accordion">
                            <dt>
                                <a href="#">Dude You Re Getting A Telescope</a>
                            </dt>
                            <dd>
                                Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque
                                mattis, leo quam aliquet diam, congue laoreet
                                elit metus eget diam. Proin ac metus diam.
                            </dd>
                            <dt>
                                <a href="#">The Basics Of Buying A Telescope</a>
                            </dt>
                            <dd>
                                Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci.
                                Aenean dignissim pellentesque felis.
                                leo quam aliquet diam, congue laoreet elit metus eget diam.
                            </dd>
                            <dt>
                                <a href="#">Astronomy Binoculars A Great Alternative</a>
                            </dt>
                            <dd>
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus hendrerit.
                                Pellentesque aliquet nibh nec urna. Proin
                                ac metus diam.
                            </dd>
                            <dt>
                                <a href="#">Do Your Self Realizations Quickly Fade</a>
                            </dt>
                            <dd>
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus hendrerit.
                                Pellentesque aliquet nibh nec urna. Proin
                                ac metus diam.
                            </dd>
                            <dt>
                                <a href="#">Success Steps For Your Personal Or Business Life</a>
                            </dt>
                            <dd>
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus hendrerit.
                                Pellentesque aliquet nibh nec urna. Proin
                                ac metus diam.
                            </dd>
                        </dl>
                    </div>
                    <div class="tab-pane fade" id="pills-support" role="tabpanel" aria-labelledby="pills-support-tab">
                        <dl class="accordion">
                            <dt>
                                <a href="#">Success Steps For Your Personal Or Business Life</a>
                            </dt>
                            <dd>
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus hendrerit.
                                Pellentesque aliquet nibh nec urna. Proin
                                ac metus diam.
                            </dd>
                            <dt>
                                <a href="#">Dude You Re Getting A Telescope</a>
                            </dt>
                            <dd>
                                Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque
                                mattis, leo quam aliquet diam, congue laoreet
                                elit metus eget diam. Proin ac metus diam.
                            </dd>
                            <dt>
                                <a href="#">The Basics Of Buying A Telescope</a>
                            </dt>
                            <dd>
                                Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci.
                                Aenean dignissim pellentesque felis.
                                leo quam aliquet diam, congue laoreet elit metus eget diam.
                            </dd>
                            <dt>
                                <a href="#">Astronomy Binoculars A Great Alternative</a>
                            </dt>
                            <dd>
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus hendrerit.
                                Pellentesque aliquet nibh nec urna. Proin
                                ac metus diam.
                            </dd>
                        </dl>
                    </div>
                    <div class="tab-pane fade" id="pills-refund" role="tabpanel" aria-labelledby="pills-refund-tab">
                        <dl class="accordion">
                            <dt>
                                <a href="#">The Basics Of Buying A Telescope</a>
                            </dt>
                            <dd>
                                Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci.
                                Aenean dignissim pellentesque felis.
                                leo quam aliquet diam, congue laoreet elit metus eget diam.
                            </dd>
                            <dt>
                                <a href="#">Astronomy Binoculars A Great Alternative</a>
                            </dt>
                            <dd>
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus hendrerit.
                                Pellentesque aliquet nibh nec urna. Proin
                                ac metus diam.
                            </dd>
                            <dt>
                                <a href="#">Do Your Self Realizations Quickly Fade</a>
                            </dt>
                            <dd>
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus hendrerit.
                                Pellentesque aliquet nibh nec urna. Proin
                                ac metus diam.
                            </dd>
                            <dt>
                                <a href="#">Success Steps For Your Personal Or Business Life</a>
                            </dt>
                            <dd>
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus hendrerit.
                                Pellentesque aliquet nibh nec urna. Proin
                                ac metus diam.
                            </dd>
                        </dl>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End faq-section -->


@endsection