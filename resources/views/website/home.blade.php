@extends('website.master')

@section('title')
    Mediusware | Home
@endsection

@section('content')

    <!-- Start hero-section -->
{{--    @include('website.include.slider')--}}
    <!-- End hero-section -->

    <!-- End team section -->
    <div id="app">
        <my-header></my-header>
        <div class="body-content">
            <router-view></router-view>
        </div>
        <my-footer></my-footer>
    </div>
@endsection
