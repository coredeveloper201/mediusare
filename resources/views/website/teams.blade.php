@extends('website.master')

@section('title')
    Mediusware | Teams
@endsection

@section('content')
    <!-- Start page-top section -->
    <section class="page-top-section">
        <div class="container">
            <div class="row justify-content-between align-items-center">
                <div class="col-lg-6 col-md-6">
                    <h1 class="text-white">Teams</h1>
                </div>
                <div class="col-lg-6  col-md-6 page-top-nav">
                    <div>
                        <a href="{!! url('/home') !!}">Home</a>
                        <span class="lnr lnr-arrow-right"></span>
                        <a href="teams.html">Teams</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End page-top section -->

    <!-- Start unique-feature Area -->
    <section class="unique-feature-area section-gap">
        <div class="container">

            <div class="row justify-content-center section-title-wrap">
                <div class="col-lg-12">
                    <div class="title-img">
                        <img src="./assets/img/title-icon.png" alt="">
                    </div>
                    <h1>Our All <span class="text-info">Expertise</span> Here</h1><br>
                    <p>Our most stylish homegrown talents on their passions</p>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-12 single-team">
                    <div class="our-team">
                        <div class="pic">
                            <img src="https://i.ibb.co/L8Pj1mg/o6EuTCT6.jpg">
                        </div>
                        <div class="team-content">
                            <h3 class="title">Dana ROBINSON</h3>
                            <span class="post">Marketing Consultant</span>
                        </div>
                        <ul class="social">
                            <li>
                                <a href="#" class="fa fa-envelope"></a>
                            </li>

                        </ul>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-12 single-team">
                    <div class="our-team">
                        <div class="pic">
                            <img src="https://i.ibb.co/L8Pj1mg/o6EuTCT6.jpg">
                        </div>
                        <div class="team-content">
                            <h3 class="title">Dana ROBINSON</h3>
                            <span class="post">Marketing Consultant</span>
                        </div>
                        <ul class="social">
                            <li>
                                <a href="#" class="fa fa-envelope"></a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-12 single-team">
                    <div class="our-team">
                        <div class="pic">
                            <img src="https://i.ibb.co/L8Pj1mg/o6EuTCT6.jpg">
                        </div>
                        <div class="team-content">
                            <h3 class="title">Dana ROBINSON</h3>
                            <span class="post">Marketing Consultant</span>
                        </div>
                        <ul class="social">
                            <li>
                                <a href="#" class="fa fa-envelope"></a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-12 single-team">
                    <div class="our-team">
                        <div class="pic">
                            <img src="https://i.ibb.co/L8Pj1mg/o6EuTCT6.jpg">
                        </div>
                        <div class="team-content">
                            <h3 class="title">Dana ROBINSON</h3>
                            <span class="post">Marketing Consultant</span>
                        </div>
                        <ul class="social">
                            <li>
                                <a href="#" class="fa fa-envelope"></a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-12 single-team">
                    <div class="our-team">
                        <div class="pic">
                            <img src="https://i.ibb.co/L8Pj1mg/o6EuTCT6.jpg">
                        </div>
                        <div class="team-content">
                            <h3 class="title">Dana ROBINSON</h3>
                            <span class="post">Marketing Consultant</span>
                        </div>
                        <ul class="social">
                            <li>
                                <a href="#" class="fa fa-envelope"></a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-12 single-team">
                    <div class="our-team">
                        <div class="pic">
                            <img src="https://i.ibb.co/L8Pj1mg/o6EuTCT6.jpg">
                        </div>
                        <div class="team-content">
                            <h3 class="title">Dana ROBINSON</h3>
                            <span class="post">Marketing Consultant</span>
                        </div>
                        <ul class="social">
                            <li>
                                <a href="#" class="fa fa-envelope"></a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-12 single-team">
                    <div class="our-team">
                        <div class="pic">
                            <img src="https://i.ibb.co/L8Pj1mg/o6EuTCT6.jpg">
                        </div>
                        <div class="team-content">
                            <h3 class="title">Dana ROBINSON</h3>
                            <span class="post">Marketing Consultant</span>
                        </div>
                        <ul class="social">
                            <li>
                                <a href="#" class="fa fa-envelope"></a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-12 single-team">
                    <div class="our-team">
                        <div class="pic">
                            <img src="https://i.ibb.co/L8Pj1mg/o6EuTCT6.jpg">
                        </div>
                        <div class="team-content">
                            <h3 class="title">Dana ROBINSON</h3>
                            <span class="post">Marketing Consultant</span>
                        </div>
                        <ul class="social">
                            <li>
                                <a href="#" class="fa fa-envelope"></a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-12 single-team">
                    <div class="our-team">
                        <div class="pic">
                            <img src="https://i.ibb.co/L8Pj1mg/o6EuTCT6.jpg">
                        </div>
                        <div class="team-content">
                            <h3 class="title">Dana ROBINSON</h3>
                            <span class="post">Marketing Consultant</span>
                        </div>
                        <ul class="social">
                            <li>
                                <a href="#" class="fa fa-envelope"></a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-12 single-team">
                    <div class="our-team">
                        <div class="pic">
                            <img src="https://i.ibb.co/L8Pj1mg/o6EuTCT6.jpg">
                        </div>
                        <div class="team-content">
                            <h3 class="title">Dana ROBINSON</h3>
                            <span class="post">Marketing Consultant</span>
                        </div>
                        <ul class="social">
                            <li>
                                <a href="#" class="fa fa-envelope"></a>
                            </li>
                        </ul>
                    </div>
                </div>


            </div>
        </div>
    </section>
    <!-- End unique-feature Area -->

@endsection