@extends('admin.master')

@section('title')
    Mediusware | Career View
@endsection

@section('content')
    <!-- Bread crumb and right sidebar toggle -->
    <div class="page-breadcrumb border-bottom">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                <h5 class="font-medium text-uppercase mb-0">Career View</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">

                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0">
                        <li class="breadcrumb-item"><a href="{!! url('/home') !!}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('home') !!}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('careers.index') !!}">Career</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Create</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <!-- End Bread crumb and right sidebar toggle -->


    <!-- Container fluid  -->

    <div class="page-content container-fluid">

        @include('admin.include.alert')

        <div class="row">
            <div class="col-12">
                <div class="material-card card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-10">
                                <h4 class="card-title text-info">{{ $career->title }}</h4>
                            </div>
                            <div class="col-2 text-right">
                                <a href="{!! route('careers.index') !!}" class="btn btn-success"><i class="fa fa-arrow-left"></i> Back</a>
                            </div>
                            <div class="col-12">
                                <hr>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <h4 class="text-info">Vacancy:</h4>
                                <p class="text-justify">{{ $career->vacancy }}</p>

                                <h4 class="text-info">Job Context:</h4>
                                <p class="text-justify">{!! $career->job_context !!}</p>

                                <h4 class="text-info">Job Responsibilities:</h4>
                                <p class="text-justify">{!! $career->job_responsibilities !!}</p>

                                <h4 class="text-info">Employment Status:</h4>
                                <p class="text-justify">{{ $career->employment_status }}</p>

                                <h4 class="text-info">Experience Requirements:</h4>
                                <p class="text-justify">{{ $career->experience_requirements }}</p>

                                <h4 class="text-info">Additional Requirements:</h4>
                                <p class="text-justify">{!! $career->additional_requirements !!}</p>

                                <h4 class="text-info">Compensation & Other Benefits:</h4>
                                <p class="text-justify">{!! $career->other_benefits !!}</p>

                                <h4 class="text-info">Salary:</h4>
                                <p class="text-justify">{{ $career->salary }}</p>

                                <h4 class="text-info">Deadline:</h4>
                                <p class="text-justify">{{ date('d M Y', strtotime($career->deadline)) }}</p>

                                <h4 class="text-info">Read Before Apply:</h4>
                                <p class="text-justify">{!! $career->read_before_apply !!}</p>


                            @if($career->image)
                                <h4 class="text-info">Image:</h4>
                                    <img src="{{ asset('media/career/'. $career->image) }}">
                                @endif
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <!-- First Cards Row  -->
    </div>


@endsection

@section('page_js')

@endsection
