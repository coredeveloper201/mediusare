<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="sidebar-item">
                    <a class="sidebar-link has-arrow waves-effect waves-dark profile-dd" href="javascript:void(0)" aria-expanded="false">
                        <img src="{!! asset('admin') !!}/images/users/1.jpg" class="rounded-circle ml-2" width="30">
                        <span class="hide-menu">{!! Auth::user()->name !!} </span>
                    </a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <li class="sidebar-item">
                            <a href="javascript:void(0)" class="sidebar-link">
                                <i class="ti-user"></i>
                                <span class="hide-menu"> My Profile </span>
                            </a>
                        </li>

                        <li class="sidebar-item">
                            <a href="javascript:void(0)" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" href="javascript:void(0)" class="sidebar-link">
                                <i class="fas fa-power-off"></i>
                                <span class="hide-menu"> Logout </span>
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </li>

                <li class="sidebar-item">
                    <a href="{!! route('home') !!}" class="sidebar-link">
                        <i class="mdi mdi-view-dashboard"></i>
                        <span class="hide-menu"> Dashboard </span>
                    </a>
                </li>
                <div class="devider"></div>

                @if(Auth::user()->role == 'admin')
                <li class="sidebar-item {!! Request::is('mediuswareadmin/user*') ? 'selected':'' !!}">
                    <a href="{!! route('user.index') !!}" class="sidebar-link {!! Request::is('mediuswareadmin/user*') ? 'active':'' !!}">
                        <i class="fa fa-users"></i>
                        <span class="hide-menu"> User </span>
                    </a>
                </li>
                @endif

                <li class="sidebar-item {!! Request::is('mediuswareadmin/service*') ? 'selected':'' !!}">
                    <a href="{!! route('services.index') !!}" class="sidebar-link {!! Request::is('mediuswareadmin/service*') ? 'active':'' !!}">
                        <i class="mdi mdi-server-network"></i>
                        <span class="hide-menu"> Services </span>
                    </a>
                </li>

                <li class="sidebar-item {!! Request::is('mediuswareadmin/categor*') ? 'selected':'' !!}">
                    <a href="{!! route('category.index') !!}" class="sidebar-link {!! Request::is('mediuswareadmin/categor*') ? 'active':'' !!}">
                        <i class="mdi mdi-cube"></i>
                        <span class="hide-menu"> Categories </span>
                    </a>
                </li>

                <li class="sidebar-item {!! Request::is('mediuswareadmin/portfolio*') ? 'selected':'' !!}">
                    <a href="{!! route('portfolio.index') !!}" class="sidebar-link {!! Request::is('mediuswareadmin/portfolio*') ? 'active':'' !!}">
                        <i class="mdi mdi-code-brackets"></i>
                        <span class="hide-menu"> Portfolios </span>
                    </a>
                </li>

                <li class="sidebar-item {!! Request::is('mediuswareadmin/team*') ? 'selected':'' !!}">
                    <a href="{!! route('team.index') !!}" class="sidebar-link {!! Request::is('mediuswareadmin/team*') ? 'active':'' !!}">
                        <i class="fa fa-users"></i>
                        <span class="hide-menu"> Teams </span>
                    </a>
                </li>

                <li class="sidebar-item {!! Request::is('mediuswareadmin/slider*') ? 'selected':'' !!}">
                    <a href="{!! route('slider.index') !!}" class="sidebar-link {!! Request::is('mediuswareadmin/slider*') ? 'active':'' !!}">
                        <i class="fa fa-sliders-h"></i>
                        <span class="hide-menu"> Sliders </span>
                    </a>
                </li>

                <li class="sidebar-item {!! Request::is('mediuswareadmin/about*') ? 'selected':'' !!}">
                    <a href="{!! route('about.index') !!}" class="sidebar-link {!! Request::is('mediuswareadmin/about*') ? 'active':'' !!}">
                        <i class="fa fa-calendar-plus"></i>
                        <span class="hide-menu"> About </span>
                    </a>
                </li>

                <li class="sidebar-item {!! Request::is('mediuswareadmin/technology*') ? 'selected':'' !!}">
                    <a href="{!! route('technology.index') !!}" class="sidebar-link {!! Request::is('mediuswareadmin/technology*') ? 'active':'' !!}">
                        <i class="fa fa-signal"></i>
                        <span class="hide-menu"> Technology </span>
                    </a>
                </li>

                <li class="sidebar-item {!! Request::is('mediuswareadmin/client*') ? 'selected':'' !!}">
                    <a href="{!! route('client.index') !!}" class="sidebar-link {!! Request::is('mediuswareadmin/client*') ? 'active':'' !!}">
                        <i class="fa fa-user-secret"></i>
                        <span class="hide-menu"> Clients </span>
                    </a>
                </li>

                <li class="sidebar-item {!! Request::is('mediuswareadmin/partner*') ? 'selected':'' !!}">
                    <a href="{!! url('mediuswareadmin/partner') !!}" class="sidebar-link {!! Request::is('mediuswareadmin/partner*') ? 'active':'' !!}">
                        <i class="fa fa-handshake"></i>
                        <span class="hide-menu"> Partners </span>
                    </a>
                </li>

                <li class="sidebar-item {!! Request::is('mediuswareadmin/feature*') ? 'selected':'' !!}">
                    <a href="{!! route('feature.index') !!}" class="sidebar-link {!! Request::is('mediuswareadmin/feature*') ? 'active':'' !!}">
                        <i class="fa fa-file-archive"></i>
                        <span class="hide-menu"> Features </span>
                    </a>
                </li>

                <li class="sidebar-item {!! Request::is('mediuswareadmin/contact*') ? 'selected':'' !!}">
                    <a href="{!! url('mediuswareadmin/contacts') !!}" class="sidebar-link {!! Request::is('mediuswareadmin/contact*') ? 'active':'' !!}">
                        <i class="mdi mdi-mailbox"></i>
                        <span class="hide-menu"> Contacts </span>
                    </a>
                </li>

                <li class="sidebar-item {!! Request::is('mediuswareadmin/informative-text*') ? 'selected':'' !!}">
                    <a href="{!! url('mediuswareadmin/informative-text') !!}" class="sidebar-link {!! Request::is('mediuswareadmin/informative-text*') ? 'active':'' !!}">
                        <i class="mdi mdi-information"></i>
                        <span class="hide-menu"> Informative Text </span>
                    </a>
                </li>

                <li class="sidebar-item {!! Request::is('mediuswareadmin/testimonial*') ? 'selected':'' !!}">
                    <a href="{!! url('mediuswareadmin/testimonial') !!}" class="sidebar-link {!! Request::is('mediuswareadmin/testimonial*') ? 'active':'' !!}">
                        <i class="fa fa-info"></i>
                        <span class="hide-menu"> Testimonial </span>
                    </a>
                </li>

                <li class="sidebar-item {!! Request::is('mediuswareadmin/setting*') ? 'selected':'' !!}">
                    <a href="{!! url('mediuswareadmin/setting') !!}" class="sidebar-link {!! Request::is('mediuswareadmin/setting*') ? 'active':'' !!}">
                        <i class="mdi mdi-settings"></i>
                        <span class="hide-menu"> Setting </span>
                    </a>
                </li>
                <li class="sidebar-item {!! Request::is('mediuswareadmin/career*') ? 'selected':'' !!}">
                    <a href="{{ route('careers.index') }}" class="sidebar-link {!! Request::is('mediuswareadmin/career*') ? 'active':'' !!}">
                        <i class="mdi mdi-blur-radial"></i>
                        <span class="hide-menu"> career </span>
                    </a>
                </li>


{{--                <li class="sidebar-item">--}}
{{--                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">--}}
{{--                        <i class="mdi mdi-cart-outline"></i>--}}
{{--                        <span class="hide-menu">Portfolios</span>--}}
{{--                    </a>--}}
{{--                    <ul aria-expanded="false" class="collapse first-level">--}}
{{--                        <li class="sidebar-item">--}}
{{--                            <a href="eco-products.html" class="sidebar-link">--}}
{{--                                <i class="mdi mdi-cards-variant"></i>--}}
{{--                                <span class="hide-menu">Categories</span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="sidebar-item">--}}
{{--                            <a href="eco-products-cart.html" class="sidebar-link">--}}
{{--                                <i class="mdi mdi-cart"></i>--}}
{{--                                <span class="hide-menu">Portfolios</span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        --}}
{{--                    </ul>--}}
{{--                </li>--}}

            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
