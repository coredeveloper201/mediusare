@extends('admin.master')

@section('title')
    Mediusware | Contact
@endsection

@section('content')
    <!-- Bread crumb and right sidebar toggle -->
    <div class="page-breadcrumb border-bottom">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                <h5 class="font-medium text-uppercase mb-0">Contact</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">

                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0">
                        <li class="breadcrumb-item"><a href="{!! url('/home') !!}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('home') !!}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Contact</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    
    <!-- End Bread crumb and right sidebar toggle -->
    
    
    <!-- Container fluid  -->

    <div class="page-content container-fluid">

    @include('admin.include.alert')
        
        <!-- First Cards Row  -->
        <div class="row">
            <div class="col-12">
                <div class="material-card card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-10">
                                <h4 class="card-title">All Contact</h4>
                            </div>
                            <div class="col-2 text-right">
                                <a href="{!! url('mediuswareadmin/dashboard') !!}" class="btn btn-success"><i class="fa fa-arrow-left"></i> Back</a>
                            </div>
                            <div class="col-12">
                                <hr>
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table id="row_create_call" class="table table-striped table-hover table-bordered display" style="width:100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th width="30%">Subject</th>
                                    <th width="40%">Message</th>
                                    <th>Ip Address</th>
                                    <th width="7%">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($contacts as $key => $contact)
                                <tr>
                                    <td>{!! ++$key !!}</td>
                                    <td>{!! $contact->name !!}</td>
                                    <td>{!! $contact->email !!}</td>
                                    <td>{!! $contact->subject !!}</td>
                                    <td>{!! $contact->message !!}</td>
                                    <td>{!! $contact->ip_address !!}</td>
                                    <td>
                                        <a href="{!! url('mediuswareadmin/contact/delete', $contact->id) !!}" onclick="return confirm('Are you sure...?')" class="btn btn-danger btn-circle"><i class="fa fa-trash"></i> </a>
                                    </td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection