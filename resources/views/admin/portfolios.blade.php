@extends('admin.master')

@section('title')
    Mediusware | Portfolios
@endsection

@section('content')
    <!-- Bread crumb and right sidebar toggle -->
    <div class="page-breadcrumb border-bottom">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                <h5 class="font-medium text-uppercase mb-0">Portfolios</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">

                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0">
                        <li class="breadcrumb-item"><a href="{!! url('/') !!}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('home') !!}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Portfolios</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <!-- End Bread crumb and right sidebar toggle -->


    <!-- Container fluid  -->

    <div class="page-content container-fluid">

    @include('admin.include.alert')

        <!-- First Cards Row  -->
        <div class="row">
            <div class="col-12">
                <div class="material-card card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-10">
                                <h4 class="card-title">All portfolio</h4>
                            </div>
                            <div class="col-md-2 text-right">
                                <a href="{!! route('portfolio.create') !!}" class="btn btn-success"><i class="fa fa-plus-circle"></i> Add New</a>
                            </div>
                            <div class="col-12">
                                <hr>
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table id="row_create_call" class="table table-striped table-hover table-bordered display" style="width:100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Title</th>
                                    <th width="30%">Project Impact</th>
                                    <th>Showcase Menu</th>
                                    <th>Status</th>
                                    <th width="10%">Action</th>
                                </tr>
                                </thead>
                                <tbody id="tablecontents">
                                    @foreach($portfolios as $key => $portfolio)
                                    <tr class="row1" data-id="{{ $portfolio->id }}">
                                        <td>{!! ++$key !!}</td>
                                        <td>{!! $portfolio->title !!}</td>
                                        <td>{!! trim(substr($portfolio->impact, 0, 100)) !!}...</td>
                                        <td>
                                            @if($portfolio->showcase == 1)
                                                <span class="badge badge-pill badge-success">Active</span>
                                            @else
                                                <span class="badge badge-pill badge-danger">Inactive</span>
                                            @endif
                                        </td>

                                        <td>
                                            @if($portfolio->status == 'active')
                                                <span class="badge badge-pill badge-success">Active</span>
                                            @else
                                                <span class="badge badge-pill badge-danger">Inactive</span>
                                            @endif
                                        </td>
                                        <td>

                                            <form action="{{ route('portfolio.destroy', $portfolio->id)}}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <a href="javascript:void(0)" data-id="{{ $portfolio->id }}" data-toggle="modal" data-target="#portfolioModal" class="btn btn-success btn-circle viewPortfolio"><i class="fa fa-eye"></i> </a>
                                                <a href="{!! url('mediuswareadmin/portfolio/'.$portfolio->id) !!}/edit" class="btn btn-info btn-circle"><i class="fa fa-edit"></i> </a>
                                                <button type="submit" onclick="return confirm('Are you sure...?')" class="btn btn-danger btn-circle"><i class="fa fa-trash"></i> </button>
                                            </form>

                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <div class="modal fade bs-example-modal-lg" id="portfolioModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">View Portfolio</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <table class="table">
                        <tr>
                            <th>Title</th>
                            <td>:</td>
                            <td id="title"></td>
                        </tr>

                        <tr>
                            <th>Service Provided</th>
                            <td>:</td>
                            <td id="service_provided"></td>
                        </tr>


                        <tr>
                            <th>Project About</th>
                            <td>:</td>
                            <td id="company_about"></td>
                        </tr>

                        <tr>
                            <th>Solution</th>
                            <td>:</td>
                            <td id="solution"></td>
                        </tr>

                        <tr>
                            <th>Impact</th>
                            <td>:</td>
                            <td id="impact"></td>
                        </tr>

                        <tr>
                            <th>Client</th>
                            <td>:</td>
                            <td id="client"></td>
                        </tr>

                        <tr>
                            <th>Project URL</th>
                            <td>:</td>
                            <td> <a target="_blank" class="myLink">View Demo</a></td>
                        </tr>

                        <tr>
                            <th>Project Image</th>
                            <td>:</td>
                            <td> <img id="myImage" src="" width="200"> </td>
                        </tr>

                    </table>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

@endsection

@section('page_js')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(".viewPortfolio").click(function (e) {
            var id = $(this).attr("data-id");
            e.preventDefault();
            $.ajax({
                type: "get",
                url: "{!! url('mediuswareadmin/portfolio') !!}/" + id,
                success: function (data) {

                    $("#title").html(data.obj.title);
                    $("#service_provided").html(data.obj.service_provided);
                    $("#company_about").html(data.obj.company_about);
                    $("#solution").html(data.obj.solution);
                    $("#impact").html(data.obj.impact);
                    $("#client").html(data.obj.client_name);
                    $("a.myLink").attr("href", data.obj.url);
                    $("#myImage").attr("src", data.image);
                    $("#category_name").html(data.category_name);

                }
            });
        });


        $(function () {

            $( "#tablecontents" ).sortable({
                items: "tr",
                cursor: 'move',
                opacity: 0.6,
                update: function() {
                    sendOrderToServer();
                }
            });

            function sendOrderToServer() {
                var order = [];

                $('tr.row1').each(function(index,element) {
                    order.push({
                        id: $(this).attr('data-id'),
                        position: index+1,
                    });
                });

                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "{{ url('mediuswareadmin/portfolio/sortable') }}",
                    data: {
                        order:order,
                        _token: '{{csrf_token()}}'
                    },
                    success: function(response) {
                        if (response.status == "success") {
                            console.log(response);
                        } else {
                            console.log(response);
                        }
                    }
                });

            }
        });



    </script>
@endsection
