@extends('admin.master')

@section('title')
    Mediusware | Client
@endsection

@section('content')
    <!-- Bread crumb and right sidebar toggle -->
    <div class="page-breadcrumb border-bottom">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                <h5 class="font-medium text-uppercase mb-0">Client</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">

                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0">
                        <li class="breadcrumb-item"><a href="{!! url('/') !!}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('home') !!}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Client</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <!-- End Bread crumb and right sidebar toggle -->


    <!-- Container fluid  -->

    <div class="page-content container-fluid">

    @include('admin.include.alert')

        <!-- First Cards Row  -->
        <div class="row">
            <div class="col-12">
                <div class="material-card card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-10">
                                <h4 class="card-title">All Client</h4>
                            </div>
                            <div class="col-md-2 text-right">
                                <a href="{!! route('client.create') !!}" class="btn btn-success"><i class="fa fa-plus-circle"></i> Add New</a>
                            </div>
                            <div class="col-12">
                                <hr>
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table id="row_create_call" class="table table-striped table-hover table-bordered display" style="width:100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th width="13%">Image</th>
                                    <th>Title</th>
                                    <th>Status</th>
                                    <th width="8%">Action</th>
                                </tr>
                                </thead>
                                <tbody id="tablecontents">
                                    @foreach($clients as $key => $client)
                                    <tr class="row1" data-id="{{ $client->id }}">
                                        <td>{!! ++$key !!}</td>
                                        <td class="text-center"><img src="{!! asset('media/client/'.$client->image) !!}" width="150"></td>
                                        <td>{!! $client->title !!}</td>
                                        <td>
                                            @if($client->status == 'active')
                                                <span class="badge badge-pill badge-success">Active</span>
                                            @else
                                                <span class="badge badge-pill badge-danger">Inactive</span>
                                            @endif
                                        </td>
                                        <td>
                                            <form action="{{ route('client.destroy', $client->id)}}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <a href="{!! url('mediuswareadmin/client/'.$client->id) !!}/edit" class="btn btn-info btn-circle"><i class="fa fa-edit"></i> </a>
                                                <button type="submit" onclick="return confirm('Are you sure...?')" class="btn btn-danger btn-circle"><i class="fa fa-trash"></i> </button>
                                            </form>

                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>




@endsection

@section('page_js')
    <script type="text/javascript">
        $(function () {
            $( "#tablecontents" ).sortable({
                items: "tr",
                cursor: 'move',
                opacity: 0.6,
                update: function() {
                    sendOrderToServer();
                }
            });
            function sendOrderToServer() {
                var order = [];

                $('tr.row1').each(function(index,element) {
                    order.push({
                        id: $(this).attr('data-id'),
                        position: index+1,
                    });
                });

                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "{{ url('mediuswareadmin/client/sortable') }}",
                    data: {
                        order:order,
                        _token: '{{csrf_token()}}'
                    },
                    success: function(response) {
                        if (response.status == "success") {
                            console.log(response);
                        } else {
                            console.log(response);
                        }
                    }
                });
            }
        });

    </script>
@endsection
