@extends('admin.master')

@section('title')
    Mediusware | Service Edit | {!! $service->title !!}
@endsection

@section('content')
    <!-- Bread crumb and right sidebar toggle -->
    <div class="page-breadcrumb border-bottom">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                <h5 class="font-medium text-uppercase mb-0">Service Edit | {!! $service->title !!}</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">

                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0">
                        <li class="breadcrumb-item"><a href="{!! url('/') !!}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('home') !!}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('services.index') !!}">Services</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Edit</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <!-- End Bread crumb and right sidebar toggle -->


    <!-- Container fluid  -->

    <div class="page-content container-fluid">

        @include('admin.include.alert')

        <div class="row">
            <div class="col-12">
                <div class="material-card card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-10">
                                <h4 class="card-title">Service Update</h4>
                            </div>
                            <div class="col-2 text-right">
                                <a href="{!! route('services.index') !!}" class="btn btn-success"><i class="fa fa-arrow-left"></i> Back</a>
                            </div>
                            <div class="col-12">
                                <hr>
                            </div>
                        </div>

                        <form class="" method="post" action="{!! route('services.update', $service->id) !!}" novalidate enctype="multipart/form-data">
                            @method('PATCH')
                            @csrf
                            <div class="row">
                                <div class="col-lg-7 col-md-7 col-xs-12">

                                    <div class="form-group">
                                        <h5>Select Technology<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <select name="technology[]" class="select2 form-control{{ $errors->has('technology') ? ' is-invalid' : '' }}" multiple="multiple" style="height: 36px;width: 100%;"  required data-validation-required-message="This field is required">
                                                @foreach($technologies as $technology)
                                                    <option value="{!! $technology->id !!}" {{ (in_array($technology->id, $service->getTechnologies))?'selected':'' }}>
                                                        {!! $technology->title !!}
                                                    </option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('title'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('title') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h5>Title<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="title" value="{!! old('title', $service->title) !!}" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                            @if ($errors->has('title'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('title') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h5>Short Description<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <textarea  name="short_description" rows="3" class="form-control{{ $errors->has('short_description') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">{!! old('short_description', $service->short_description) !!}</textarea>
                                            @if ($errors->has('short_description'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('short_description') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h5>Description Left<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <textarea  name="description_left" class="form-control{{ $errors->has('description_left') ? ' is-invalid' : '' }} summernote" required data-validation-required-message="This field is required">{!! old('description_left', $service->description_left) !!}</textarea>
                                            @if ($errors->has('description_left'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('description_left') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h5>Description Right<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <textarea  name="description_right" class="form-control{{ $errors->has('description_right') ? ' is-invalid' : '' }} summernote" required data-validation-required-message="This field is required">{!! old('description_right', $service->description_right) !!}</textarea>
                                            @if ($errors->has('description_right'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('description_right') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h5>Icon<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" id="iconName" data-toggle="modal" data-target=".bs-example-modal-lg" name="icon" value="{!! old('icon', $service->icon) !!}" class="form-control{{ $errors->has('icon') ? ' is-invalid' : '' }}" placeholder="Font Awesome Icon: fa fa-user" required data-validation-required-message="This field is required">
                                            @if ($errors->has('icon'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('icon') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h5>Publish Date<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="date" value="{!! old('date', $service->date) !!}" class="form-control{{ $errors->has('date') ? ' is-invalid' : '' }}" autocomplete="off" required data-validation-required-message="This field is required" id="datepicker-autoclose" placeholder="yyyy-mm-dd">
                                            @if ($errors->has('date'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('date') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h5>Image Cover Image (1903x600)</h5>
                                        <div class="controls">
                                            <input type="file" name="image" class="form-control{{ $errors->has('image') ? ' is-invalid' : '' }}">
                                            <br>
                                            <img src="{!! asset('media/service/'. $service->image) !!}" width="100">
                                            @if ($errors->has('image'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('image') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h5>Industries Image</h5>
                                        <div class="controls">
                                            <input type="file" name="industries_image" class="form-control{{ $errors->has('industries_image') ? ' is-invalid' : '' }}">
                                            <br>
                                            <img src="{!! asset('media/industries/'. $service->industries_image) !!}" width="100">
                                            @if ($errors->has('industries_image'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('industries_image') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group validate">
                                        <h5>Status <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <fieldset>
                                                <div class="custom-control custom-radio">
                                                    <input type="radio" name="status" value="active" {!! $service->status == 'active' ? 'checked':''  !!} required="" id="status1" class="custom-control-input" aria-invalid="false">
                                                    <label class="custom-control-label" for="status1">Active</label>
                                                </div>
                                            </fieldset>
                                            <fieldset>
                                                <div class="custom-control custom-radio">
                                                    <input type="radio"  name="status" value="inactive" {!! $service->status == 'inactive' ? 'checked':''  !!} id="status" class="custom-control-input" aria-invalid="false">
                                                    <label class="custom-control-label" for="status">Inactive</label>
                                                </div>
                                            </fieldset>
                                            @if ($errors->has('status'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('status') }}</strong>
                                                </span>
                                            @endif
                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                    <hr>
                                </div>

                                <div class="col-lg-5 col-md-5 col-xs-12">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <h5>Offered Services<span class="text-danger">*</span></h5>
                                                <div class="controls">
                                                    <input type="text" name="offered_services_title[]" value="{!! old('offered_services_title') !!}" placeholder="" class="form-control{{ $errors->has('offered_services_title') ? ' is-invalid' : '' }}">
                                                    @if ($errors->has('offered_services_title'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('offered_services_title') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <br>
                                            <div class="form-group">
                                                <button class="btn btn-success" type="button" onclick="offeredServices();"><i class="fa fa-plus"></i></button>
                                            </div>
                                        </div>
                                    </div>

                                    @if(!empty($service->getServiceOffered))
                                        @php $o = 1 @endphp
                                        @foreach($service->getServiceOffered as $key => $offered)
                                            <div class="form-group removeOfferedServices{{$o}}">
                                                <div class="row">
                                                    <div class="col-md-10">
                                                        <div class="form-group">
                                                            <div class="controls">
                                                                <input type="text" name="offered_services_title[]" value="{!! old('offered_services_title', $offered->offered_services_title) !!}" placeholder="" class="form-control{{ $errors->has('offered_services_title') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                                                @if ($errors->has('portfolio_feature'))
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $errors->first('offered_services_title') }}</strong>
                                                                    </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <div class="form-group">
                                                            <button class="btn btn-danger" type="button" onclick="removeOfferedServices({!! $o !!});"><i class="fa fa-minus"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @php $o++ @endphp
                                        @endforeach
                                    @endif

                                    <div style="margin-top: 15px!important;" id="offeredServices"></div>
                                    <hr>

                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <h5>Industries<span class="text-danger">*</span></h5>
                                                <div class="controls">
                                                    <input type="text" name="industries_title[]" value="{!! old('industries_title') !!}" placeholder="" class="form-control{{ $errors->has('industries') ? ' is-invalid' : '' }}">
                                                    @if ($errors->has('industries_title'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('industries_title') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <br>
                                            <div class="form-group">
                                                <button class="btn btn-success" type="button" onclick="industries();"><i class="fa fa-plus"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    @if(!empty($service->getServiceIndustries))
                                        @php $i = 1 @endphp
                                        @foreach($service->getServiceIndustries as $key => $industries)
                                            <div class="form-group removeIndustries{{$i}}">
                                                <div class="row">
                                                    <div class="col-md-10">
                                                        <div class="form-group">
                                                            <div class="controls">
                                                                <input type="text" name="industries_title[]" value="{!! old('industries_title', $industries->services_industries_title) !!}" placeholder="" class="form-control{{ $errors->has('industries_title') ? ' is-invalid' : '' }}">
                                                                @if ($errors->has('industries_title'))
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $errors->first('industries_title') }}</strong>
                                                                    </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <div class="form-group">
                                                            <button class="btn btn-danger" type="button" onclick="removeIndustries({!! $i !!});"><i class="fa fa-minus"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @php $i++ @endphp
                                        @endforeach
                                    @endif

                                    <div style="margin-top: 15px!important;" id="industries"></div>
                                </div>
                            </div>


                            <div class="text-xs-right">
                                <button type="submit" class="btn btn-info">Update</button>
                                <button type="reset" class="btn btn-inverse">Reset</button>
                            </div>
                        </form>


                    </div>
                </div>
            </div>
        </div>
        <!-- First Cards Row  -->
    </div>

    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">Font Awesome Icon</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    @include('admin.include.icon')
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>

    </div>

@endsection
@section('page_js')
    <script>
        $(document).ready(function () {
            $('.f-icon').click(function(){
                var icon = $(this).find('i').attr("class");
                $("#iconName").val(icon);
                $('.bs-example-modal-lg').modal('hide');
            })
        });
    </script>

    <script>
        var inc = 1;

        function industries() {
            inc++;
            var objTo = document.getElementById('industries')
            var divtest = document.createElement("div");
            divtest.setAttribute("class", "form-group removeOfferedServices" + inc);
            var rdiv = 'removeOfferedServices' + inc;
            divtest.innerHTML =
                '<div class="row">'+
                '<div class="col-md-10">'+
                '<div class="form-group">'+
                '<div class="controls">'+
                '<input type="text" name="industries_title[]" value="" placeholder="" class="form-control" required data-validation-required-message="This field is required">'+

                '</div>'+
                '</div>'+
                '</div>'+


                '<div class="col-sm-2"> ' +
                '<div class="form-group"> ' +
                '<button class="btn btn-danger" type="button" onclick="removeOfferedServices(' + inc + ');"> <i class="fa fa-minus"></i> </button> ' +
                '</div>' +
                '</div>'+
                '</div>';

            objTo.appendChild(divtest)
        }

        function removeOfferedServices(rid) {
            $('.removeOfferedServices' + rid).remove();
        }
    </script>

    <script>
        var industriesInc = 1;

        function offeredServices() {
            industriesInc++;
            var objTo = document.getElementById('offeredServices')
            var divtest = document.createElement("div");
            divtest.setAttribute("class", "form-group removeIndustries" + industriesInc);
            var rdiv = 'removeIndustries' + industriesInc;
            divtest.innerHTML =
                '<div class="row">'+
                '<div class="col-md-10">'+
                '<div class="form-group">'+
                '<div class="controls">'+
                '<input type="text" name="offered_services_title[]" value="" placeholder="" class="form-control" required data-validation-required-message="This field is required">'+

                '</div>'+
                '</div>'+
                '</div>'+

                '<div class="col-sm-2"> ' +
                '<div class="form-group"> ' +
                '<button class="btn btn-danger" type="button" onclick="removeIndustries(' + industriesInc + ');"> <i class="fa fa-minus"></i> </button> ' +
                '</div>' +
                '</div>'+
                '</div>';

            objTo.appendChild(divtest)
        }

        function removeIndustries(rid) {
            $('.removeIndustries' + rid).remove();
        }
    </script>
@endsection
